let productos_seleccionados = [];

function mostrar(){
    document.getElementById("boton_pedido").disabled = true;
    document.getElementById("boton_vaciar").disabled = true;
    let data= new Date();
        let hora= data.getHours();
        let min= data.getMinutes();
        console.log(hora);
        console.log(min);
        if ((hora>=12 || (hora==11 && min>30)) && (hora<16 || (hora==16 && min<=30))){
            document.getElementById("dinar").style.display = 'block';

        }else{
            document.getElementById("productos").style.display = 'block';
        }
}


function captar_id(id, precio,que_hacer)
{      
    document.getElementById("boton_pedido").disabled = false;
    document.getElementById("boton_vaciar").disabled = false;
    let nombre=document.getElementById(id).getElementsByClassName("producto_nombre")[0].textContent;
    if(que_hacer)
    {
        anadir_al_array(id,nombre,precio);    
    }else if(!que_hacer){
        borrar_del_array(id);
    }
    actualizar_cesta();
}

function anadir_al_array(id,nombre,precio){
    let found=false;
    for (let i = 0; i < productos_seleccionados.length; i++) {
        if(productos_seleccionados[i][0]==id){
            productos_seleccionados[i][3]+=1;
            found=true;
        }      
    }
    if(!found){
        productos_seleccionados[productos_seleccionados.length]= [id,nombre,precio,1];
    }
    
}
function borrar_del_array(id){
    let found=false;
    for (let i = 0; i < productos_seleccionados.length && !found; i++) {
        if(productos_seleccionados[i][0]==id){
            if (productos_seleccionados[i][3]>1) {
                productos_seleccionados[i][3]-=1;
            } else {
                productos_seleccionados.splice(i,1);    
            }
            found=true;
        }      
    }
}

function actualizar_cesta(){
    let precioPedido=0;
    let element = document.getElementById("cesta");
    element.innerHTML = '';
    for (let i = 0; i < productos_seleccionados.length; i++) {
        let precioProducto= (productos_seleccionados[i][2] * productos_seleccionados[i][3]);
        precioProducto= parseFloat(precioProducto.toFixed(2));
        precioPedido+=precioProducto; console.log(precioPedido)
        let cesta = document.createElement("p");
        let node = document.createTextNode(productos_seleccionados[i][3]+" x " +productos_seleccionados[i][1]+": "+precioProducto+"€");
        cesta.appendChild(node);
        cesta.setAttribute('id', productos_seleccionados[i][0])
        let element = document.getElementById("cesta");
        element.appendChild(cesta);    

    }
    precioPedido= precioPedido.toFixed(2);
    if(precioPedido<=0){
        document.getElementById("boton_pedido").disabled = true;
        document.getElementById("boton_vaciar").disabled = true;
    }
    document.getElementById("precio_pedido").innerHTML = "Total a pagar: "+precioPedido+"€";
    
}

function vaciar_cesta() {
    productos_seleccionados=[];
    actualizar_cesta();
    document.getElementById("boton_pedido").disabled = true;
    document.getElementById("boton_vaciar").disabled = true;
    
}

function llenar_formulario(){
    let cesta = [[],[]];
    for (let i = 0; i < productos_seleccionados.length; i++) {
        cesta[i][0]=productos_seleccionados[i][0];
        cesta[i][1]=productos_seleccionados[i][3];
    }
    var prod_seleccionados_cadena= cesta.toString();
    document.getElementById('codigos_cesta').value = prod_seleccionados_cadena;
}


// Cuenta atras para la pantalla de error


    let day= new Date();
    let hour = 23-day.getHours();
    let minuts = 59-day.getMinutes();
    let secons = 59 - day.getSeconds();
    window.onload = updateClock;
    
    
    
    function updateClock() {
      document.getElementById('countdown').innerHTML = hour+"H : "+minuts+"M : "+secons+"s";
        if(secons!=0){
        secons-=1;
        setTimeout("updateClock()",1000);
        if(secons==0){
            minuts--;
            secons=59;
        }
        if(minuts==0){
            hour--;
            minuts=59;
            secons=59;
        }
    }
    }