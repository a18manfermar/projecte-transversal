<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Administrador</title>
</head>
<body>
<?php
    include 'header.php'

/**
 * Funcion que muestra la estructura de carpetas a partir de la ruta dada.
 */

function obtener_estructura_directorios($ruta){
    // Se comprueba que realmente sea la ruta de un directorio
    if (is_dir($ruta)){
        // Abre un gestor de directorios para la ruta indicada
        $gestor = opendir($ruta);
        // Recorre todos los elementos del directorio
        while (($archivo = readdir($gestor)) !== false)  {
                
            $ruta_completa = $ruta . "/" . $archivo;
            $ruta_archivo="../Comandas/".$archivo;
            //visualizamos el contenido de los archivos 
            $imprimir=shell_exec("cat $ruta_archivo ");
            // Se muestran todos los archivos y carpetas excepto "." y ".."
            if ($archivo != "." && $archivo != "..") {
                // Si es un directorio se recorre recursivamente
                if (is_dir($ruta_completa)) {

                    
                    echo "<h1>". $archivo ."</h1>";
                    echo "<p>$imprimir</p>";

                    obtener_estructura_directorios($ruta_completa);
                } else {
                    echo "<h1>". $archivo."</h1>";
                    echo "<p>$imprimir</p>";
                }
            }
        }
        
        // Cierra el gestor de directorios
        closedir($gestor);
    } else {
        echo "No es una ruta de directorio valida<br/>";
    }
}
$ruta="../Comandas";
obtener_estructura_directorios($ruta);
include 'footer.php';
?>
 <button onclick="location='index.php'">Ir a inicio</button>
</body>
</html>