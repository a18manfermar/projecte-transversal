## Intergrants del grup
El grup esta format per **Alexey Rud, Laia Rodès, Manel Ferrer**

---

## Objectiu

Desenvolupament d'una aplicació web per fer comandes a la cantina.

Els usuaris unicament poden fer una comanda al dia.

L'administrador te un directori protegit per accedir a les comandes.

No es pot fer us de SQL per magatzemar els productes i les seves caracteristiques 

---

## Estat del projecte

**Acabado**

flujo de pantallas

footer i header se insertan en todas las paginas

cesta mostrando los productos al usuario

se comprueba que el correo tiene formarto mail 

el formulario genera al submit


**Falta desplegar**

comprobar que el nombre no este vacio ni tenga numeros

comprobar que el telefono sea numerico i de 9 cifras

mejora grafica para el usuario 

---

## Adreça web del projecte

[labs.iam.cat/~a18manfermar/projecte-transversal](http://labs.iam.cat/~a18manfermar/projecte-transversal)