<!DOCTYPE html>
<?php
include "cookie.php";
set_cookie();
?>
<html lang="es">
<?php include 'header.html';?> 
<body>
<?php
	
//posem el dia,mes i any en un string => 22-10-2019
$fecha=idate("d")."-".idate("m")."-".idate("Y");
$carpeta="./comanda/".$fecha;
$id=count(glob($carpeta.'/*.txt',GLOB_BRACE));
$FCesta="COMPRA:\n";


$cFecha="Fecha: ".$fecha."\n\n";
$nom="NOM: ".$_POST["nom"]."\n";
$telf="TELÉFON: ".$_POST["tel"]."\n";
$correu="CORREU: ".$_POST["correu"]."\n";
require 'contenidoMenu.php';
if($_POST["cesta"]==null){
    $FCesta="Menú de 7.5€";
}
else{
    $lista = explode (',', $_POST["cesta"]);

$numPedidos= count($lista);

$total = 0;
    for($i=0; $i<$numPedidos; $i++){
        if ($i==0 || ($i%2)==0){
            $imprimir= $lista[$i];
        }
        else{
            foreach ($menuEsmorzar as $menjar => $cod){
                if ($menjar==$lista[$i-1]){
                    foreach($cod as $producte => $resposta){
                        if ($producte=='nom'){
                            $FCesta= $FCesta. $resposta;
                        }
                        else if ($producte=='preu'){
                            $imprimir= $lista[$i];
                            $FCesta= $FCesta." $resposta € x $imprimir\n\n";
                            $total+= $resposta*$lista[$i];

                        }
                    }
                }
            }
        }
    }

    
    $FCesta= $FCesta."\n\nTotal: $total \n\n\n";
}
$cadena=$cFecha.$nom.$telf.$correu."\n\n".$FCesta;
//file_exists es per comprobar si existeix el directori

if(file_exists($carpeta)){

    chdir($carpeta);
    
    $fp=fopen($id."-".$fecha.".txt","w");
    fwrite($fp,$cadena);
    fclose($fp);
    
}else{
    
    mkdir($carpeta);
    
    chdir($carpeta);
    
    $fp=fopen($id."-".$fecha.".txt","w");
    fwrite($fp,$cadena);
    fclose($fp);
}
?>
        <p>Has finalitzat la comanda</p>
        <button onclick="location='index.php'">Volver a pagina inicial</button> 

        <?php include 'footer.html';?>
</body>
</html>