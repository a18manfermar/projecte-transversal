<?php include 'header.html';?> 
    <body onload="mostrar()">
        <div id="productos_cesta">        
            <div class="titulo_y_productos">
                <div id="productos">
                <h3>PRODUCTOS DESAYUNO</h3>
                    <div class="productos">
                    <?php
                    //para acceder a fotos 
                    const VAL_MIN_BOCATAS=0000;
                    const VAL_MAX_BOCATAS_FREDS=1200;
                    const VAL_MAX_BOCATAS=1300;
                    const VAL_MIN_BEBIDAS=2100;
                    const VAL_MAX_BEBIDAS_FREDAS=2200;
                    const VAL_MAX_BEBIDAS=2300;
                    const PREU_B_DELDIA=1.7;
                    $bocataDelDia=0;
                    $bocatasDelDia = array();
                    $cont=0;
                    
                    require 'contenidoMenu.php';
                    foreach ($menuEsmorzar as $menjar => $cod){
                        if ($menjar>VAL_MIN_BOCATAS && $menjar <VAL_MAX_BOCATAS_FREDS) {
                            foreach($cod as $producte => $resposta){
                                if ($producte=='preu' && $resposta==PREU_B_DELDIA){
                                    $bocataDelDia=1;
                                }
                                if ($bocataDelDia==1){
                                $bocatasDelDia[$cont]=$menuEsmorzar[$menjar];
                                $cont++;
                                $bocataDelDia=0;
                                }
                            }
                        }
                    }

                    $numBocatas= count($bocatasDelDia);
                    $numBocatas--;
                    $numero=rand (0 , $numBocatas);
                    $titol=0;
                    foreach ($bocatasDelDia[$numero] as $menjar => $nom){
                        if ($menjar=='nom'){
                            $nom_producte=$nom;
                            $foto ="1101";
                        } 
                    }
                    echo "<div class='producto' id='0000'> 
                                <img src='img/$foto.jpg' alt='$nom_producte'> 
                                <p class='producto_nombre'> Del dia: $tipus $nom_producte</p> 
                                <p class='producto_precio'> 1.5 € </p> 
                                <div class='iconos_add_remove'>
                                    <img onClick='captar_id(0000,1.5, true)' src='img/add.png' alt='add product'>
                                    <img onClick='captar_id(0000,1.5, false)' src='img/remove.png' alt='remove product'>
                                </div>
                            </div>";
                            foreach ($menuEsmorzar as $menjar => $cod){
                                if ($menjar>VAL_MIN_BOCATAS && $menjar <VAL_MAX_BOCATAS) {
                                    if ($menjar>VAL_MIN_BOCATAS && $menjar <VAL_MAX_BOCATAS_FREDS && $titol==0){
                                        $titol=1;
                                        
                                    }else if ($menjar>VAL_MAX_BOCATAS_FREDS && $menjar <VAL_MAX_BOCATAS && $titol==1){
                                        $titol=2;
                                    }
                                    $tipus="Entrepà de ";
                                    if($titol==1){
                                        $foto ="1101"; 
                                    }else{
                                        $foto ="1201";
                                    }
                                }else {
                                    if ($menjar>VAL_MIN_BEBIDAS && $menjar <VAL_MAX_BEBIDAS_FREDAS && $titol==2){
                                        $titol=3;
                                    }else if ($menjar>VAL_MAX_BEBIDAS_FREDAS && $menjar <VAL_MAX_BEBIDAS && $titol==3){
                                        $titol=4;
                                    }
                                    $tipus="";
                                    if($titol==3){
                                        $foto =$menjar;
                                    }else{
                                        $foto ='2201';
                                    }
                                    
                                    

                                }
                                foreach($cod as $producte => $resposta){
                                        if ($producte=='nom'){
                                                $nom_producte=$resposta;
                                        }else if($producte=='preu'){
                                                    $preu=$resposta;
                                        }
                                }

                                //echo "<div class='producto' id='$menjar' onClick='captar_id(this.id, $nom_producte, $preu )'> 
                                echo "<div class='producto' id='$menjar' > 
                                            <img src='img/$foto.jpg' alt='$nom_producte'> 
                                            <p class='producto_nombre'> $tipus $nom_producte</p> 
                                            <p class='producto_precio'> $preu € </p> 
                                            <div class='iconos_add_remove'>
                                                <img onClick='captar_id($menjar,$preu, true)' src='img/add.png' alt='add product'>
                                                <img onClick='captar_id($menjar,$preu, false)' src='img/remove.png' alt='remove product'>
                                            </div>
                                    </div>";
                                
                            
                            }
                    ?>
                    </div>
                </div>
                <div id="dinar">
                <h3>MENÚ DEL DIA</h3>
                    <?php
                    const PLATS =3;
                    const VAL_PLAT1=100;
                    const VAL_PLAT2=200;
                    const VAL_PLAT3=300;
                    const VAL_MAX_DINAR=400;
                    
                    for($i=0; $i<PLATS; $i++){
                        $cont=0;
                        $dinar = array();
                        foreach ($menuDinar as $menjar => $cod){
                            if($i==0){
                                if ($menjar>VAL_PLAT1 && $menjar <VAL_PLAT2) {
                                    $dinar[$cont]=$menuDinar[$menjar];
                                    $cont++;
                                }
                            }else if($i==1){
                                if ($menjar>VAL_PLAT2 && $menjar <VAL_PLAT3) {
                                    $dinar[$cont]=$menuDinar[$menjar];
                                    $cont++;
                                }
                            }else{
                                if ($menjar>VAL_PLAT3 && $menjar <VAL_MAX_DINAR) {
                                    $dinar[$cont]=$menuDinar[$menjar];
                                    $cont++;
                                }
                            }
                        }
                        $numPlats= count($dinar);
                        $numPlats--;
                        $numero=rand (0 , $numPlats);
                        $plat=$dinar[$numero];
                        foreach ($dinar[$numero] as $menjar => $nom){
                            if($menjar=='nom'){
                                $alimento=$nom;
                            }else if($menjar=='especificitats'){
                                $esp=$nom;
                            }
                        }
                        echo "<p especificitat='$esp'>$alimento</p>";
                    }
                    ?>
                    <p>Preu 7.5 €</p>
                    <button onclick="location='formalitzar.php'">Comprar Menu</button>
                </div>
            </div>

            <div  class="titulo_y_cesta">
                <h3>CESTA</h3>
                <div id="cesta"> 
                
                </div>
                <p id="precio_pedido">Total a pagar: 0€</p>
                <form action="formalitzar.php" method="post">
                    <input type="hidden" name="codigos" id="codigos_cesta"><br>
                <button id="boton_pedido" onclick="llenar_formulario()" disabled>Realizar pedido</button>             
                </form>
                <button id="boton_vaciar" onclick="vaciar_cesta()">Vaciar cesta</button> 
            </div>
        </div>

       
    <?php include 'footer.html';?>
