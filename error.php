    <?php
        include 'header.html';
           echo $header;
    ?>
    <body>
    <div class="row space-top-8 space-8 row-table">
        <div class="col-5 col-middle">
            <h1>Error, ya has realizado un pedido hoy</h1>
            <br/><br/><br/>
            <h4>Podrá realizar su proximo pedido dentro de: <span id="countdown"></span>  </h4>
    </div>
    <div class="col-2 col-middle">
        <img src="img/error.gif" width="100" height="350">
    </div>
</div>
<div class="row align-items-start">
    <div class="col"></div>
    <button onclick="location='index.php'" class="btn btn-outline-primary col-1">Ir a inicio</button>
    <div class="col"></div>
  </div>
  <br/><br/>
    <?php
       include 'footer.html';
           echo $footer;
    ?>

</body>
</html>
