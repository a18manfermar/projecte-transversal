<?php
	$menuEsmorzar = array(
	    1101 => array('nom'=>"formatge", 'preu'=> 1.7, 'imatge'=>"", 'especificitats' => "glv"),
        1102 => array('nom'=>"pernil dolç", 'preu'=>1.7, 'imatge'=>"", 'especificitats' =>"g"),
        1103 => array('nom'=>"pernil salat", 'preu'=>1.9, 'imatge'=>"", 'especificitats' =>"g"),
        1104 => array('nom'=>"fuet de Vic", 'preu'=>1.7, 'imatge'=>"", 'especificitats' =>"g"),
        1105 => array('nom'=>"xoriço", 'preu'=>1.7, 'imatge'=>"", 'especificitats' =>"g"),
        1106 => array('nom'=>"tonyina", 'preu'=>2.0, 'imatge'=>"", 'especificitats' =>"g"),
        1107 => array('nom'=>"vegetal tonyina", 'preu'=>3.0, 'imatge'=>"", 'especificitats' =>"g"),
        1108 => array('nom'=>"vegetal pollastre", 'preu'=>3.0, 'imatge'=>"", 'especificitats' =>"g"),
        1109 => array('nom'=>"bull blanc", 'preu'=>2.0, 'imatge'=>"", 'especificitats' =>"g"),
        1110 => array('nom'=>"gall d'indi", 'preu'=>1.7, 'imatge'=>"", 'especificitats' =>"g"),
        1111 => array('nom'=>"nutella", 'preu'=>1.8, 'imatge'=>"", 'especificitats' =>"gv"),

        1201 => array('nom'=>"bacon", 'preu'=>2.0, 'imatge'=>"", 'especificitats' =>"g"),
        1202 => array('nom'=>"frankfurt", 'preu'=>2.0, 'imatge'=>"", 'especificitats' =>"g"),
        1203 => array('nom'=>"truita", 'preu'=>2.5, 'imatge'=>"", 'especificitats' =>"gv"),
        1204 => array('nom'=>"llom", 'preu'=>2.4, 'imatge'=>"", 'especificitats' =>"g"),
        1205 => array('nom'=>"pollastre", 'preu'=>2.4, 'imatge'=>"", 'especificitats' =>"g"),
        1206 => array('nom'=>"xistorra", 'preu'=>2.0, 'imatge'=>"", 'especificitats' =>"g"),
        1207 => array('nom'=>"hamburguesa", 'preu'=>2.4, 'imatge'=>"", 'especificitats' =>"g"),
        1208 => array('nom'=>"mallorquí", 'preu'=>2.5, 'imatge'=>"", 'especificitats' =>"gl"),

        2101 => array('nom'=>"Aigua gran", 'preu'=>1.0, 'imatge'=>"", 'especificitats' =>null),
        2102 => array('nom'=>"Aigua petita", 'preu'=>0.7, 'imatge'=>"", 'especificitats' =>null),
        2103 => array('nom'=>"Fanta limon", 'preu'=>1.2, 'imatge'=>"", 'especificitats' =>""),
        2104 => array('nom'=>"Fanta naranja", 'preu'=>1.2, 'imatge'=>"", 'especificitats' =>""),
        2105 => array('nom'=>"Batut de chocolata", 'preu'=>1.2, 'imatge'=>"", 'especificitats' =>""),
        2106 => array('nom'=>"Cacaolat", 'preu'=>1.2, 'imatge'=>"", 'especificitats' =>""),
        2201 => array('nom'=>"Cafe sol", 'preu'=>1.0, 'imatge'=>"", 'especificitats' =>null),
        2202 => array('nom'=>"Cafe amb llet", 'preu'=>0.7, 'imatge'=>"", 'especificitats' =>null),
        2203 => array('nom'=>"Té", 'preu'=>1.2, 'imatge'=>"", 'especificitats' =>""),
        2204 => array('nom'=>"Café amb gel", 'preu'=>1.2, 'imatge'=>"", 'especificitats' =>""),
        2205 => array('nom'=>"Infusió", 'preu'=>1.2, 'imatge'=>"", 'especificitats' =>""),

    );
    $menuDinar = array(
	    101 => array('nom'=>"sopa", 'especificitats' =>"glv"),
        102 => array('nom'=>"macarrons", 'especificitats' =>"g"),
        201 => array('nom'=>"bistec", 'especificitats' =>"glv"),
        202 => array('nom'=>"truita", 'especificitats' =>"g"),
        301 => array('nom'=>"manzana", 'especificitats' =>"glv"),
        302 => array('nom'=>"pera", 'especificitats' =>"g"),
        
        );
?>